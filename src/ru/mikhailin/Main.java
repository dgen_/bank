package ru.mikhailin;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Bank bank = new Bank();

        while (true) {
            showMenu();
            int option = scanner.nextInt();
            scanner.nextLine();

            switch (option) {
                case 1:
                    bank.addBranch();
                    break;

                case 2:
                    System.out.print("Enter branch number: ");
                    int branchNumber = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Enter customer name: ");
                    String customerName = scanner.nextLine();
                    System.out.print("Enter initial transaction: ");
                    double transaction = scanner.nextDouble();
                    scanner.nextLine();
                    bank.addCustomerToBranch(branchNumber, customerName, transaction);
                    break;

                case 3:
                    System.out.print("Enter branch number: ");
                    branchNumber = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Enter customer name: ");
                    customerName = scanner.nextLine();
                    System.out.print("Enter initial transaction: ");
                    transaction = scanner.nextDouble();
                    scanner.nextLine();
                    bank.addTransactionToCustomerInBranch(branchNumber, customerName, transaction);
                    break;

                case 4:
                    System.out.print("Enter branch number: ");
                    branchNumber = scanner.nextInt();
                    scanner.nextLine();
                    bank.listCustomersInBranch(branchNumber, false);
                    break;

                case 5:
                    System.out.print("Enter branch number: ");
                    branchNumber = scanner.nextInt();
                    scanner.nextLine();
                    bank.listCustomersInBranch(branchNumber, true);
                    break;

                case 6:
                    return;
            }
        }
    }

    public static void showMenu() {
        System.out.println();
        System.out.println("Choose your option:");
        System.out.println("1. Add branch");
        System.out.println("2. Add customer to branch");
        System.out.println("3. Add a transaction to the existing customer");
        System.out.println("4. List all customers in the branch");
        System.out.println("5. Show all customers with their transactions");
        System.out.println("6. Exit");
        System.out.println();
    }
}
