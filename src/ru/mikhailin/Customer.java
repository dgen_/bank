package ru.mikhailin;

import java.util.ArrayList;

public class Customer {
    private String name;
    private ArrayList<Double> transactions;

    public Customer(String name, double transaction) {
        this.transactions = new ArrayList<>();
        this.transactions.add(transaction);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }

    public void addTransaction(double transaction) {
        transactions.add(transaction);
    }

    public void listTransactions() {
        System.out.println("List of transactions for customer " + name + ":");
        for (Double transaction : transactions) {
            System.out.println(transaction);
        }
    }
}
