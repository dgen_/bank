package ru.mikhailin;

import java.util.ArrayList;

public class Bank {
    private ArrayList<Branch> branches;

    public Bank() {
        this.branches = new ArrayList<>();
        System.out.println("Bank created!");
    }

    public void addBranch() {
        branches.add(new Branch());
        System.out.println("Branch #" + branches.size() + " added");
    }

    public boolean addCustomerToBranch(int branchNumber, String name, double transaction) {
        if (branches.size() < branchNumber) {
            System.out.println("Branch does not exist");
            return false;
        }

        if (branches.get(branchNumber - 1).addNewCustomer(name, transaction)) {
            System.out.println("New Customer " + name + " with transaction " + transaction + " added");
            return true;
        }

        System.out.println("Customer " + name + " already exists");
        return false;
    }

    public boolean addTransactionToCustomerInBranch(int branchNumber, String name, double transaction) {
        if (branches.size() < branchNumber) {
            System.out.println("Branch does not exist");
            return false;
        }

        for (Customer customer : branches.get(branchNumber - 1).getCustomers()) {
            if (customer.getName().equals(name)) {
                customer.addTransaction(transaction);
                System.out.println("Transaction " + transaction + " added to customer " + name);
                return true;
            }
        }

        System.out.println("Customer does not exist");
        return false;
    }

    public void listCustomersInBranch(int number, boolean showTransactions) {
        if (branches.size() < number) {
            System.out.println("Branch does not exist");
        } else {
            for (Customer customer : branches.get(number - 1).getCustomers()) {
                System.out.println(customer.getName());
                if (showTransactions) {
                    customer.listTransactions();
                }
            }
        }

    }
}
