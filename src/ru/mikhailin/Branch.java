package ru.mikhailin;

import java.util.ArrayList;

public class Branch {
    private ArrayList<Customer> customers;

    public Branch() {
        customers = new ArrayList<>();
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public boolean addNewCustomer(String name, double transaction) {
        for (Customer customer : customers) {
            if (customer.getName().equals(name)) {
                return false;
            }
        }

        customers.add(new Customer(name, transaction));
        return true;
    }

    public boolean addTransaction(String name, double transaction) {
        for (Customer customer : customers) {
            if (customer.getName().equals(name)) {
                customer.addTransaction(transaction);
                return true;
            }
        }

        return false;
    }
}
